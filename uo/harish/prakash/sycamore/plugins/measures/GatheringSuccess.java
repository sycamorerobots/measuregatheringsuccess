/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.measures;

import java.util.Iterator;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;
import it.diunipi.volpi.sycamore.plugins.measures.MeasureImpl;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class GatheringSuccess extends MeasureImpl {

	private String	_author				= "Harish Prakash";
	private String	_shortDescription	= "Print Gathering Success";
	private String	_description		= "Print The number of distinct positions occupied by all the robots";
	private String	_pluginName			= "GatheringSuccess";

	@Override
	public void onSimulationStart() {}

	@Override
	public void onSimulationStep() {}

	@Override
	public void onSimulationEnd() {

		@SuppressWarnings("unchecked")
		Iterator<SycamoreRobot<Point2D>> robots = engine.getRobots().iterator();
		Point2D previous = null;

		while (robots.hasNext()) {
			SycamoreRobot<Point2D> robot = robots.next();
			Point2D robotPosition = robot.getGlobalPosition();

			if (robot.getAlgorithm().getPluginName().equals("DetectableCrashedRobot")) {
				continue;
			}

			if (robot.getAlgorithm().getPluginName().equals("CrashedRobot")) {
				continue;
			}

			else if (previous == null) {
				previous = robotPosition;
				continue;
			}

			else if (robotPosition.distanceTo(previous) > 100 * SycamoreSystem.getEpsilon()) {
				System.out.println(0);
				return;
			}
		}

		System.out.println(1);
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

}
